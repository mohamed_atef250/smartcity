package com.yumaas.smartcity.base.models;


import java.io.Serializable;
import java.util.ArrayList;

import com.yumaas.smartcity.base.DataBaseHelper;

public class Ads implements Serializable {
    public int id;
    public String title,details,image;
    public ArrayList<Comment> comments=new ArrayList<>();

    public Ads(String title, String details, String image){
        this.id= DataBaseHelper.getCounter();
        this.title=title;
        this.details=details;
        this.image=image;
    }

}
