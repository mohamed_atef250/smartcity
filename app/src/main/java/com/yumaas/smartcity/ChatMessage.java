package com.yumaas.smartcity;



public class ChatMessage {


	private int doctorId;


	private String updatedAt;


	private int userId;


	private int sender;


	private String createdAt;


	private int id;


	private String message;

	private String type;


	private int clinicId;

	public void setDoctorId(int doctorId){
		this.doctorId = doctorId;
	}

	public int getDoctorId(){
		return doctorId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setSender(int sender){
		this.sender = sender;
	}

	public int getSender(){
		return sender;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setClinicId(int clinicId){
		this.clinicId = clinicId;
	}

	public int getClinicId(){
		return clinicId;
	}

	@Override
 	public String toString(){
		return 
			"ChatMessage{" + 
			"doctor_id = '" + doctorId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",user_id = '" + userId + '\'' + 
			",sender = '" + sender + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",message = '" + message + '\'' + 
			",type = '" + type + '\'' + 
			",clinic_id = '" + clinicId + '\'' + 
			"}";
		}
}