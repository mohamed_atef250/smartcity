package com.yumaas.smartcity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.yumaas.smartcity.base.UserFavouritesPlacesFragment;

import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;

public class UserMainActivity extends AppCompatActivity {

    TextView title;
    SmoothBottomBar smoothBottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);
        title = findViewById(R.id.title);

        FragmentHelper.replaceFragment(this, new UserPlacesFragment(), "AdminHomeFragment");

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {

                FragmentHelper.popAllFragments(UserMainActivity.this);

                if (i == 0) {
                    title.setText("الصفحة الرئيسيه");
                    FragmentHelper.replaceFragment(UserMainActivity.this, new UserPlacesFragment(), "AdminHomeFragment");
                } else if (i == 1) {
                    title.setText("المفضله");
                    FragmentHelper.replaceFragment(UserMainActivity.this, new UserFavouritesPlacesFragment(), "UserFavouritesFragment");
                }
                else if (i == 2) {
                    title.setText("الدليل");
                    FragmentHelper.replaceFragment(UserMainActivity.this, new UserPhonesFragment(), "UserPhonesFragment");
                } else {
                    FragmentHelper.replaceFragment(UserMainActivity.this, new ProfileFragment(), "ProfileFragment");

                }


                return false;
            }
        });


    }

}