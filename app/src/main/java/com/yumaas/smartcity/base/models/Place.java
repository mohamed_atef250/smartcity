package com.yumaas.smartcity.base.models;

import java.io.Serializable;

import com.yumaas.smartcity.base.DataBaseHelper;

public class Place implements Serializable {

    public int id;
    public String name,phone,image;


    public Place(String name, String phone, String image) {
        this.id = DataBaseHelper.getCounter();
        this.name = name;
        this.phone=phone;
        this.image=image;


    }
}
