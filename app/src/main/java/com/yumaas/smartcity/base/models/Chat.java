package com.yumaas.smartcity.base.models;

import java.io.Serializable;

public class Chat implements Serializable {

    public User reciver;
    public User sender;
    public String message;
    public String type;

    public Chat(User reciver, User sender, String message, String type) {
        this.reciver = reciver;
        this.sender = sender;
        this.message = message;
        this.type=type;
    }
}
