package com.yumaas.smartcity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;

public class ManagerMainActivity extends AppCompatActivity {

    TextView title;
    SmoothBottomBar smoothBottomBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_main);
        title = findViewById(R.id.title);

        FragmentHelper.replaceFragment(this,new UpdatePlaceProfile(),"UpdatePlaceProfile");

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(i -> {

            FragmentHelper.popAllFragments(ManagerMainActivity.this);
             if(i==0){
                title.setText("الرئيسه");
                FragmentHelper.replaceFragment(ManagerMainActivity.this,new UpdatePlaceProfile(),"AcceptedPlacesFragment");
            }else {
                title.setText("شكاوي ومقترحات");
                FragmentHelper.replaceFragment(ManagerMainActivity.this,new ChattersFragment2(),"ChattersFragment");
            }

            return false;
        });


    }

}