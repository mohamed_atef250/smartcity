package com.yumaas.smartcity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AcceptedPlacesAdapter extends RecyclerView.Adapter<AcceptedPlacesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<String> images;


    public AcceptedPlacesAdapter(OnItemClickListener onItemClickListener, ArrayList<String> images) {
        this.onItemClickListener = onItemClickListener;
        this.images = images;

    }


    @Override
    public int getItemCount() {
        return images == null ? 0 : images.size();
    }


    @Override
    public AcceptedPlacesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_accepted_place, parent, false);
        AcceptedPlacesAdapter.ViewHolder viewHolder = new AcceptedPlacesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AcceptedPlacesAdapter.ViewHolder holder, final int position) {

        Picasso.with(holder.itemView.getContext()
        ).load(images.get(position)).into(holder.imageView);


    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;



        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);



        }
    }
}