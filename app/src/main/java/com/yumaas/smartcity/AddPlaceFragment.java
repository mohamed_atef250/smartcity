package com.yumaas.smartcity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.SweetDialogs;
import com.yumaas.smartcity.base.filesutils.FileOperations;
import com.yumaas.smartcity.base.filesutils.VolleyFileObject;
import com.yumaas.smartcity.base.models.ImageResponse;
import com.yumaas.smartcity.base.models.Place;
import com.yumaas.smartcity.base.volleyutils.ConnectionHelper;
import com.yumaas.smartcity.base.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class AddPlaceFragment extends Fragment {

    View rootView;
    ImageView imageView;
    EditText name,phone;
    TextView image;
    Button add;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_add_place, container, false);


        name = rootView.findViewById(R.id.name);
        phone = rootView.findViewById(R.id.phone);
        image = rootView.findViewById(R.id.image);
        imageView = rootView.findViewById(R.id.imageView);


        add = rootView.findViewById(R.id.add);

        image.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataBaseHelper.addCenter(new Place(name.getText().toString(),phone.getText().toString(),selectedImage));
                SweetDialogs.successMessage(getActivity(), "تم الاضافه بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                    FragmentHelper.popAllFragments(getActivity());
                    FragmentHelper.addFragment(getActivity(),new AdminPhonesFragment(),"AdminPhonesFragment");
                    }
                });
            }
        });

        return rootView;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(getActivity(), data, "image",
                            43);
            volleyFileObjects.add(volleyFileObject);
            addServiceApi();
        } catch (Exception E) {
            E.getStackTrace();
        }


    }
    String selectedImage;

    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();
                ConnectionHelper.loadImage(imageView,selectedImage);
                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }



}