package com.yumaas.smartcity.base;


import java.util.ArrayList;

import com.yumaas.smartcity.base.models.Ads;
import com.yumaas.smartcity.base.models.Place;
import com.yumaas.smartcity.base.models.Chat;
import com.yumaas.smartcity.base.models.Child;
import com.yumaas.smartcity.base.models.Comment;
import com.yumaas.smartcity.base.models.Expert;
import com.yumaas.smartcity.base.models.Game;
import com.yumaas.smartcity.base.models.Question;
import com.yumaas.smartcity.base.models.User;
import com.yumaas.smartcity.base.models.Video;

public class DataLists {

    public ArrayList<User> users = new ArrayList<>();
    public ArrayList<Question> companies = new ArrayList<>();
    public ArrayList<Expert> experts = new ArrayList<>();
    public ArrayList<Game> games= new ArrayList<>();
    public ArrayList<Child> children= new ArrayList<>();
    public ArrayList<Video> videos= new ArrayList<>();
    public ArrayList<Chat> chats= new ArrayList<>();
    public ArrayList<Comment> comments= new ArrayList<>();
    public ArrayList<Ads> ads = new ArrayList<>();
    public ArrayList<Place> places = new ArrayList<Place>();

}
