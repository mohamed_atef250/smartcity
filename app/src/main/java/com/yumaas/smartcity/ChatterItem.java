package com.yumaas.smartcity;




public class ChatterItem {


	private int visible;


	private String updatedAt;


	private int userId;


	private String createdAt;

	private String clinicLogo;


	private int id;


	private ChatMessage chatMessage;


	private int clinicId;


	private String chatClinic;

	public void setVisible(int visible){
		this.visible = visible;
	}

	public int getVisible(){
		return visible;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setClinicLogo(String clinicLogo){
		this.clinicLogo = clinicLogo;
	}

	public String getClinicLogo(){
		return clinicLogo;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setChatMessage(ChatMessage chatMessage){
		this.chatMessage = chatMessage;
	}

	public ChatMessage getChatMessage(){
		return chatMessage;
	}

	public void setClinicId(int clinicId){
		this.clinicId = clinicId;
	}

	public int getClinicId(){
		return clinicId;
	}

	public void setChatClinic(String chatClinic){
		this.chatClinic = chatClinic;
	}

	public String getChatClinic(){
		return chatClinic;
	}

	@Override
 	public String toString(){
		return 
			"ChatterItem{" +
			"visible = '" + visible + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",user_id = '" + userId + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",clinic_logo = '" + clinicLogo + '\'' + 
			",id = '" + id + '\'' + 
			",chat_message = '" + chatMessage + '\'' + 
			",clinic_id = '" + clinicId + '\'' + 
			",chat_clinic = '" + chatClinic + '\'' + 
			"}";
		}
}