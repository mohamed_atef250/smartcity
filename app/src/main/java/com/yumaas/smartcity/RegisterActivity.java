package com.yumaas.smartcity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.HashMap;
import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.SweetDialogs;
import com.yumaas.smartcity.base.filesutils.FileOperations;
import com.yumaas.smartcity.base.filesutils.VolleyFileObject;
import com.yumaas.smartcity.base.models.ImageResponse;
import com.yumaas.smartcity.base.models.User;
import com.yumaas.smartcity.base.volleyutils.ConnectionHelper;
import com.yumaas.smartcity.base.volleyutils.ConnectionListener;



public class RegisterActivity  extends AppCompatActivity {
 
    private String selectedImage;
    private ImageView image;
    private TextView imageupload;
    private EditText name, userName, email, phone, password;
    Button add;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
 


        name = findViewById(R.id.name);
        userName = findViewById(R.id.user_name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);

        password = findViewById(R.id.password);
        image = findViewById(R.id.imageView);
        imageupload = findViewById(R.id.image);

        add = findViewById(R.id.add);

        imageupload.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        image.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        add.setOnClickListener(view -> {
            if(validate()){
            User user = new User(name.getText().toString(), userName.getText().toString(), phone.getText().toString(), email.getText().toString(), password.getText().toString(),selectedImage,"user");
                user.type="user";
            DataBaseHelper.addUser(user);
            DataBaseHelper.saveUser(user);
            Toast.makeText(this, "تمت التسجيل بنجاح", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(RegisterActivity.this, UserMainActivity.class));
            }
        });


        
        
    }


    private boolean validate(){
        if(Validate.isEmpty(name.getText().toString())
                ||Validate.isEmpty(email.getText().toString())
                ||Validate.isEmpty(phone.getText().toString())  ){
            SweetDialogs.errorMessage(com.yumaas.smartcity.RegisterActivity.this,"من فضلك قم بملآ جميع البيانات");
            return false;

        }else if(!Validate.isAvLen(password.getText().toString(),7,100)){
            SweetDialogs.errorMessage(com.yumaas.smartcity.RegisterActivity.this,"كلمة المرور يجب ان تكون اكبر من ٦ حروف او ارقام ");
            return  false;
        }else if(DataBaseHelper.findChildCheck(email.getText().toString())){
            SweetDialogs.errorMessage(com.yumaas.smartcity.RegisterActivity.this,"هذا المستخدم موجود من قبل");
            return  false;
        }


        return true;
    }


  

    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(this, data, "image",
                            43);
            volleyFileObjects.add(volleyFileObject);
            addServiceApi();
        } catch (Exception E) {
            E.getStackTrace();
        }


    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();
                ConnectionHelper.loadImage(image,selectedImage);
                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }




}
