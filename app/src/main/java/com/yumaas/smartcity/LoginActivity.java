package com.yumaas.smartcity;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.EditText;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.SweetDialogs;
import com.yumaas.smartcity.base.models.User;


public class
LoginActivity extends AppCompatActivity {

    EditText email,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.CALL_PHONE},2342);
        }

            email = findViewById(R.id.email);
          password = findViewById(R.id.password);

        findViewById(R.id.register).setOnClickListener(view -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setMessage("من فضلك قم باختيار نوع الحساب التي تريد انشاءه")
                    .setCancelable(false)
                    .setPositiveButton("مسئول دائره", (dialog, id) -> {
                        startActivity(new Intent( LoginActivity.this, RegisterPlaceActivity.class));
                        finish();
                    })
                    .setNegativeButton("مستخدم", (dialog, id) -> {
                        startActivity(new Intent( LoginActivity.this, RegisterActivity.class));
                        finish();
                    });
            AlertDialog alert = builder.create();
            alert.show();



        });





        findViewById(R.id.btn).setOnClickListener(view -> {
            if(email.getText().toString().equals("admin")){
                startActivity(new Intent( LoginActivity.this, AdminMainActivity.class));
            }else {

                User user = DataBaseHelper.loginUser(email.getText().toString()
                        , password.getText().toString());
                if (user != null) {
                    DataBaseHelper.saveUser(user);

                    Intent intent = new Intent( LoginActivity.this, UserMainActivity.class);

                    if(user.type.equals("place")){
                        intent = new Intent( LoginActivity.this, ManagerMainActivity.class);
                    }

                    startActivity(intent);
                    finish();
                } else {
                    SweetDialogs.errorMessage(LoginActivity.this, "ناسف البريد الالكتروني او كلمه السر خطا");
                }
            }



        });


    }
}
