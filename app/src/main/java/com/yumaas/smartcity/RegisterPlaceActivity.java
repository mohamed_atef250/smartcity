package com.yumaas.smartcity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.SweetDialogs;
import com.yumaas.smartcity.base.filesutils.CompressObject;
import com.yumaas.smartcity.base.filesutils.FileOperations;
import com.yumaas.smartcity.base.filesutils.VolleyFileObject;
import com.yumaas.smartcity.base.models.ImageResponse;
import com.yumaas.smartcity.base.models.User;
import com.yumaas.smartcity.base.volleyutils.ConnectionHelper;
import com.yumaas.smartcity.base.volleyutils.ConnectionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;


public class RegisterPlaceActivity extends AppCompatActivity {
 
    private String selectedImage;
    private ImageView  imageView ;
    private EditText name, userName, email, phone, password,files,location,address,facebook,insta;
    private TextView image;
    double lat=0.0, lng=0.0 ;
    Button add;
    ArrayList<VolleyFileObject> volleyFileObjects;
    List<Uri> docPaths;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_place);

        imageView=findViewById(R.id.imageView);
        files=findViewById(R.id.files);
        name = findViewById(R.id.name);
        userName = findViewById(R.id.user_name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        files = findViewById(R.id.files);
        password = findViewById(R.id.password);
        location = findViewById(R.id.location);
        image = findViewById(R.id.image);
        location =   findViewById(R.id.location);
        address = findViewById(R.id.address);
        facebook = findViewById(R.id.facebook);
        insta = findViewById(R.id.instgram
        );

        add = findViewById(R.id.btn);

        files.setFocusable(false);
        files.setClickable(true);


        image.setFocusable(false);
        image.setClickable(true);

        location.setFocusable(false);
        location.setClickable(true);



        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(RegisterPlaceActivity.this,SelectLocationActivity.class);
                startActivityForResult(intent, 150);
            }
        });



        files.setOnClickListener(view -> {

            file = true;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        image.setOnClickListener(view -> {
            file = false;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });


        add.setOnClickListener(view -> {
            if(validate()){
            User user = new User(name.getText().toString(),
                    userName.getText().toString(),
                    phone.getText().toString(), email.getText().toString(),
                    password.getText().toString(),selectedImage,"place");
                user.address = address.getText().toString();
                user.files = selectedFile;
                user.facebook=facebook.getText().toString();
                user.insta = insta.getText().toString();
                user.lat=lat;
                user.lng=lng;
                user.address = address.getText().toString();

            DataBaseHelper.addUser(user);
            DataBaseHelper.saveUser(user);
            Toast.makeText(this, "تمت الاضافه بنجاح", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(RegisterPlaceActivity.this, ManagerMainActivity.class));
            }
        });

    }


    private boolean validate(){
        if(Validate.isEmpty(name.getText().toString())
                ||Validate.isEmpty(email.getText().toString())
                ||Validate.isEmpty(phone.getText().toString())  ){
            SweetDialogs.errorMessage(RegisterPlaceActivity.this,"من فضلك قم بملآ جميع البيانات");
            return false;

        }else if(!Validate.isAvLen(password.getText().toString(),7,100)){
            SweetDialogs.errorMessage(RegisterPlaceActivity.this,"كلمة المرور يجب ان تكون اكبر من ٦ حروف او ارقام ");
            return  false;
        }else if(DataBaseHelper.findChildCheck(email.getText().toString())){
            SweetDialogs.errorMessage(RegisterPlaceActivity.this,"هذا المستخدم موجود من قبل");
            return  false;
        }


        return true;
    }


  


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            VolleyFileObject volleyFileObject = null;
            volleyFileObjects = new ArrayList<>();


            if (requestCode == 150) {
                location.setText("تم اختيار الموقع بنجاح");

                lat = data.getDoubleExtra("lat", 0.0);
                lng = data.getDoubleExtra("lng", 0.0);
            } else {

                try {

                    volleyFileObject =
                            FileOperations.getVolleyFileObject(this, data, "image",
                                    43);

                } catch (Exception E) {
                    E.getStackTrace();
                }

                volleyFileObjects.add(volleyFileObject);
                addServiceApi();
            }
        }catch (Exception e){
            e.getStackTrace();
        }

    }

String selectedFile;
    boolean file;
    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;
                if(file){
                    selectedImage = imageResponse.getState();
                    files.setText("تم الاختيار بنجاح");
                }else {
                    selectedFile = imageResponse.getState();
                    ConnectionHelper.loadImage(imageView,selectedImage);
                }


                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }




}
