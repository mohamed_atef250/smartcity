package com.yumaas.smartcity.base;

public interface OnItemClickListener {
    public void onItemClickListener(int position);
}
