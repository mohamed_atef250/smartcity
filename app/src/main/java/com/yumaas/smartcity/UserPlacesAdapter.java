package com.yumaas.smartcity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.like.LikeButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class UserPlacesAdapter extends RecyclerView.Adapter<UserPlacesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<String> images;
    boolean liked=false;


    public UserPlacesAdapter(OnItemClickListener onItemClickListener, ArrayList<String> images,boolean liked) {
        this.onItemClickListener = onItemClickListener;
        this.images = images;
        this.liked=liked;
    }


    @Override
    public int getItemCount() {
        return images == null ? 0 : images.size();
    }


    @Override
    public UserPlacesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_place, parent, false);
        UserPlacesAdapter.ViewHolder viewHolder = new UserPlacesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserPlacesAdapter.ViewHolder holder, final int position) {

        Picasso.with(holder.itemView.getContext()
        ).load(images.get(position)).into(holder.imageView);

        holder.chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(),ChatViewTestActivity.class);
                view.getContext().startActivity(intent);
            }
        });

        holder.likeButton.setLiked(liked);

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,chat;
        LikeButton likeButton;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            chat = view.findViewById(R.id.chat);
            likeButton = view.findViewById(R.id.star_button);
        }
    }
}