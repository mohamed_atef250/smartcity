package com.yumaas.smartcity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ChatUsersAdapter extends RecyclerView.Adapter<ChatUsersAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<String> images;


    public ChatUsersAdapter(OnItemClickListener onItemClickListener, ArrayList<String> images) {
        this.onItemClickListener = onItemClickListener;
        this.images = images;

    }


    @Override
    public int getItemCount() {
        return images == null ? 0 : images.size();
    }


    @Override
    public ChatUsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatter, parent, false);
        ChatUsersAdapter.ViewHolder viewHolder = new ChatUsersAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ChatUsersAdapter.ViewHolder holder, final int position) {

        Picasso.with(holder.itemView.getContext()
        ).load(images.get(position)).into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(),ChatViewTestActivity.class);
                view.getContext().startActivity(intent);
            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;



        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);



        }
    }
}