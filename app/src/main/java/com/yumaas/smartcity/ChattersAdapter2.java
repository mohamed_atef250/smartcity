package com.yumaas.smartcity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.smartcity.base.models.Chat;
import com.yumaas.smartcity.base.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class ChattersAdapter2 extends RecyclerView.Adapter<ChattersAdapter2.ViewHolder> {

    Context context;
    ArrayList<Chat>chattersList;


    public ChattersAdapter2(Context context, ArrayList<Chat>chattersList) {
        this.context = context;
        this.chattersList=chattersList;
    }


    @Override
    public ChattersAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatter, parent, false);
        ChattersAdapter2.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChattersAdapter2.ViewHolder holder, final int position) {

        holder.message.setText(chattersList.get(position).reciver.name);


        ConnectionHelper.loadImage(holder.image,chattersList.get(position).reciver.image);


        holder.itemView.setOnClickListener(view -> {

            ChatFragment2 chatFragment =   new ChatFragment2();
            Bundle bundle = new Bundle();
            bundle.putSerializable("chatItem",chattersList.get(position));
            chatFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(),chatFragment, "ChatFragment");
        });

    }

    @Override
    public int getItemCount() {
        return chattersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.tv_review_reviewer);
            image = itemView.findViewById(R.id.image);
        }
    }
}
