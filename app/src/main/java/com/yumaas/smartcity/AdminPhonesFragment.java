package com.yumaas.smartcity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.models.Place;

import java.util.ArrayList;


public class AdminPhonesFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_phones, container, false);

        Button add= rootView.findViewById(R.id.add);

        add.setVisibility(View.VISIBLE);
        add.setOnClickListener(view -> {
            FragmentHelper.addFragment(getActivity(),new AddPlaceFragment(),"AddPlaceFragment");
        });

        ArrayList<Place>places = DataBaseHelper.getDataLists().places;

        final UserPhonesAdapter placesAdapter = new UserPhonesAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, places,true);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVerticalGrid(getActivity(), programsList,2);
        programsList.setAdapter(placesAdapter);


        return rootView;
    }
}