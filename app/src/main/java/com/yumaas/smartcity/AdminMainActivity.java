package com.yumaas.smartcity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;

public class AdminMainActivity extends AppCompatActivity {

    TextView title;
    SmoothBottomBar smoothBottomBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);
        title = findViewById(R.id.title);

        FragmentHelper.replaceFragment(this,new AdminHomeFragment(),"AdminHomeFragment");

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {

                FragmentHelper.popAllFragments(AdminMainActivity.this);
                if(i==0){
                    title.setText("الصفحة الرئيسيه");
                    FragmentHelper.replaceFragment(AdminMainActivity.this,new AdminHomeFragment(),"AdminHomeFragment");
                }else {
                    title.setText("الدليل");
                    FragmentHelper.replaceFragment(AdminMainActivity.this,new AdminPhonesFragment(),"AcceptedPlacesFragment");
                }

                return false;
            }
        });


    }

}