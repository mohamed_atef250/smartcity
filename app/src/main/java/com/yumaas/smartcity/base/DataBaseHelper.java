package com.yumaas.smartcity.base;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import com.yumaas.smartcity.base.models.Admin;
import com.yumaas.smartcity.base.models.Ads;
import com.yumaas.smartcity.base.models.Place;
import com.yumaas.smartcity.base.models.Chat;
import com.yumaas.smartcity.base.models.Child;
import com.yumaas.smartcity.base.models.Comment;
import com.yumaas.smartcity.base.models.Expert;
import com.yumaas.smartcity.base.models.Game;
import com.yumaas.smartcity.base.models.Question;
import com.yumaas.smartcity.base.models.User;
import com.yumaas.smartcity.base.models.Video;
import com.yumaas.smartcity.base.volleyutils.MyApplication;


public class DataBaseHelper {

    private static SharedPreferences sharedPreferences = null;


    private DataBaseHelper() {

    }


    public static boolean findChildCheck(String email) {

        DataLists dataLists = getDataLists();
        ArrayList<Child> children = dataLists.children;
        if (children != null) {
            for (int i = 0; i < dataLists.children.size(); i++) {
                if (dataLists.children.get(i).email.equals(email)) {
                    return true;
                }
            }


        }

        return false;
    }



    public static boolean findUserCheck(String email) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).email.equals(email)) {
                    return true;
                }
            }


        }

        return false;
    }




    public static void editJob(Child child) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Child> children = dataLists.children;
        if (children != null) {

            dataLists.children = children;

            for (int i = 0; i < dataLists.children.size(); i++) {
                if (dataLists.children.get(i).id.equals(child.id)) {
                    dataLists.children.set(i, child);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }


    public static void addAds(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ads == null) {
            ads = new ArrayList<>();
        }
        ads.add(ad);
        dataLists.ads = ads;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.apply();
    }




    public static void removeAd(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ad != null) {
            dataLists.ads = ads;
            for (int i = 0; i < dataLists.ads.size(); i++) {
                if (dataLists.ads.get(i).id==ad.id) {
                    dataLists.ads.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.apply();
        }
    }





    public static void addQuestion(String id, Question question) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Game> games = dataLists.games;
        if (games != null) {

            dataLists.games = games;

            for (int i = 0; i < dataLists.games.size(); i++) {
                if (dataLists.games.get(i).id.equals(id)) {
                    dataLists.games.get(i).addQuestion(question);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }


    public static SharedPreferences getSharedPreferenceInstance() {
        if (sharedPreferences != null) return sharedPreferences;
        return sharedPreferences =  MyApplication.getInstance().getApplicationContext().getSharedPreferences("savedData", Context.MODE_PRIVATE);
    }


    public static void addChat(Chat chat) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Chat> chats = dataLists.chats;
        if (chats == null) {
            chats = new ArrayList<>();
        }
        chats.add(chat);
        dataLists.chats = chats;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }



    public static void addComment(Comment comment) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Comment> comments = dataLists.comments;
        if (comments == null) {
            comments = new ArrayList<>();
        }
        comments.add(comment);
        dataLists.comments = comments;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }


    public static void addUser(User user) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users == null) {
            users = new ArrayList<>();
        }
        users.add(user);
        dataLists.users = users;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    public static void addCompany(Question question) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Question> companies = dataLists.companies;
        if (companies == null) {
            companies = new ArrayList<>();
        }
        companies.add(question);
        dataLists.companies = companies;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }


    public static void addChild(Child child) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Child> children = dataLists.children;
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(child);
        dataLists.children = children;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    public static void addExpert(Expert expert) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Expert> experts = dataLists.experts;
        if (experts == null) {
            experts = new ArrayList<>();
        }
        experts.add(expert);
        dataLists.experts = experts;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }


    public static void addGame(Game game) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Game> games = dataLists.games;
        if (games == null) {
            games = new ArrayList<>();
        }
        games.add(game);
        dataLists.games = games;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }
    public static void addVideo(Video video) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Video> videos = dataLists.videos;
        if (videos == null) {
            videos = new ArrayList<>();
        }
        videos.add(video);
        dataLists.videos = videos;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }




    public static Question findCompany(String id) {

        DataLists dataLists = getDataLists();
        ArrayList<Question> companies = dataLists.companies;
        if (companies != null) {
            for (int i = 0; i < dataLists.companies.size(); i++) {
                if (dataLists.companies.get(i).id.equals(id)) {
                    return dataLists.companies.get(i);
                }
            }
        }

        return null;
    }




    public static User findUser(int id) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id==id) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }

    public static User findUser(String userName, String password) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).userName.equals(userName) && dataLists.users.get(i).password.equals(password)) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }


    public static Child findChild(String userName, String password) {

        DataLists dataLists = getDataLists();
        ArrayList<Child> children = dataLists.children;
        if (children != null) {
            for (int i = 0; i < dataLists.children.size(); i++) {
                if (dataLists.children.get(i).userName.equals(userName) && dataLists.children.get(i).password.equals(password)) {
                    return dataLists.children.get(i);
                }
            }
        }

        return null;
    }


    public static void removeUser(User user) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {

            dataLists.users = users;

            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id==user.id) {
                    dataLists.users.remove(i);
                    break;
                }
            }
            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }

    public static void removeGame(Game game) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Game> games = dataLists.games;
        if (games != null) {

            dataLists.games = games;

            for (int i = 0; i < dataLists.games.size(); i++) {
                if (dataLists.games.get(i).id.equals(game.id)) {
                    dataLists.games.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }

    public static void removeVideo(Video video) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Video> videos = dataLists.videos;
        if (videos != null) {

            dataLists.videos = videos;

            for (int i = 0; i < dataLists.videos.size(); i++) {
                if (dataLists.videos.get(i).id.equals(video.id)) {
                    dataLists.videos.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }

    public static void removeSepecialization(User user) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {

            dataLists.users = users;

            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id==user.id) {
                    dataLists.users.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }


    public static void removeJob(Child child) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Child> children = dataLists.children;
        if (children != null) {

            dataLists.children = children;

            for (int i = 0; i < dataLists.children.size(); i++) {
                if (dataLists.children.get(i).id.equals(child.id)) {
                    dataLists.children.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }


    public static void removeUser(Question question) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Question> companies = dataLists.companies;
        if (companies != null) {
            dataLists.companies = companies;
            for (int i = 0; i < dataLists.companies.size(); i++) {
                if (dataLists.companies.get(i).id.equals(question.id)) {
                    dataLists.companies.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }


    public static User getSavedUser() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("savedUser", "");
        return gson.fromJson(json, User.class);
    }

    public static Admin getSavedAdmin() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("savedAdmin", "");
        return gson.fromJson(json, Admin.class);
    }

    public static void saveUser(User user) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("savedUser", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }


    public static Question getSavedCompany() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("savedCompany", "");

        return gson.fromJson(json, Question.class);
    }

    public static void saveCompany(Question question) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(question);
        prefsEditor.putString("savedCompany", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    public static void saveAdmin(Admin admin) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(admin);
        prefsEditor.putString("savedAdmin", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }


    public static DataLists getDataLists() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("userDetails", "");
        if (json.equals("")) return new DataLists();
        return gson.fromJson(json, DataLists.class);
    }


    public static  int getCounter() {
        int lastCounter = 0;
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();

        try {
            lastCounter = getSharedPreferenceInstance().getInt("counter", 0);
        } catch (Exception e) {
            e.getStackTrace();
        }
        prefsEditor.putInt("counter", lastCounter + 1);
        prefsEditor.commit();
        prefsEditor.apply();
        return  lastCounter+1;
    }



    public static void addCenter(Place place) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Place> places = dataLists.places;
        if (places == null) {
            places = new ArrayList<>();
        }
        places.add(place);
        dataLists.places = places;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    public static void updateCenter(Place place) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Place> places = dataLists.places;
        if (places != null) {

            dataLists.places = places;

            for (int i = 0; i < dataLists.places.size(); i++) {
                if (dataLists.places.get(i).id== place.id) {
                    dataLists.places.set(i, place);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.apply();
        }
    }



    public static void updateAds(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ads != null) {

            dataLists.ads = ads;

            for (int i = 0; i < dataLists.ads.size(); i++) {
                if (dataLists.ads.get(i).id== ad.id) {
                    dataLists.ads.set(i, ad);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.apply();
        }
    }


    public static void updateUser(User user) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (user != null) {

            dataLists.users = users;

            for (int i = 0; i < dataLists.users.size(); i++) {

                Log.e("test",user.id+" "+dataLists.users.get(i).id);

                if (dataLists.users.get(i).id== user.id) {
                    dataLists.users.set(i, user);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.apply();
        }
    }


    public static void removeCenter(Place place) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Place> places = dataLists.places;
        if (places != null) {

            dataLists.places = places;

            for (int i = 0; i < dataLists.places.size(); i++) {
                if (dataLists.places.get(i).id== place.id) {
                    dataLists.places.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }

    public static User loginUser(String email, String password) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).email.equals(email)&&dataLists.users.get(i).password.equals(password)) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }



}
