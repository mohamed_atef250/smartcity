package com.yumaas.smartcity.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.smartcity.OnItemClickListener;
import com.yumaas.smartcity.PlacesAdapter;
import com.yumaas.smartcity.R;
import com.yumaas.smartcity.ViewOperations;
import com.yumaas.smartcity.base.models.User;

import java.util.ArrayList;


public class UserFavouritesPlacesFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_admin_home_fragment, container, false);

        ArrayList<User> users = new ArrayList<>();
        ArrayList<User> usersTemp = DataBaseHelper.getDataLists().users;
        User user = DataBaseHelper.getSavedUser();
        for(int i=0; i<usersTemp.size(); i++){
            if(usersTemp.get(i).type.equals("place")&&user.checkFavourites(usersTemp.get(i).id)){
                users.add(usersTemp.get(i));
            }
        }

        final PlacesAdapter placesAdapter = new PlacesAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, users,false);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(placesAdapter);


        return rootView;
    }
}