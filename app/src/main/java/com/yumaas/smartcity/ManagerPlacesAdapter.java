package com.yumaas.smartcity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.like.LikeButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ManagerPlacesAdapter extends RecyclerView.Adapter<ManagerPlacesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<String> images;
    boolean liked=false;


    public ManagerPlacesAdapter(OnItemClickListener onItemClickListener, ArrayList<String> images, boolean liked) {
        this.onItemClickListener = onItemClickListener;
        this.images = images;
        this.liked=liked;
    }


    @Override
    public int getItemCount() {
        return images == null ? 0 : images.size();
    }


    @Override
    public ManagerPlacesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_manager_place, parent, false);
        ManagerPlacesAdapter.ViewHolder viewHolder = new ManagerPlacesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ManagerPlacesAdapter.ViewHolder holder, final int position) {

        Picasso.with(holder.itemView.getContext()
        ).load(images.get(position)).into(holder.imageView);



    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);

        }
    }
}