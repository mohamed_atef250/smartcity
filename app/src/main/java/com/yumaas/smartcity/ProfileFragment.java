package com.yumaas.smartcity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.SweetDialogs;
import com.yumaas.smartcity.base.filesutils.FileOperations;
import com.yumaas.smartcity.base.filesutils.VolleyFileObject;
import com.yumaas.smartcity.base.models.ImageResponse;
import com.yumaas.smartcity.base.models.User;
import com.yumaas.smartcity.base.volleyutils.ConnectionHelper;
import com.yumaas.smartcity.base.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;


public class ProfileFragment extends Fragment {

    View rootView;
    private String selectedImage;
    private ImageView image;
    private TextView imageupload;
    private EditText name, userName, email, phone, address, password;
    Button add;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_update_profile, container, false);


        name = rootView.findViewById(R.id.name);
        userName = rootView.findViewById(R.id.user_name);
        email = rootView.findViewById(R.id.email);
        phone = rootView.findViewById(R.id.phone);
        address = rootView.findViewById(R.id.address);
        password = rootView.findViewById(R.id.password);
        image = rootView.findViewById(R.id.imageView);
        imageupload = rootView.findViewById(R.id.image);

        setData();

        add = rootView.findViewById(R.id.add);

        imageupload.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        image.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        add.setOnClickListener(view -> {
            if (validate()) {
                User user = new User(name.getText().toString(), userName.getText().toString(), phone.getText().toString(), email.getText().toString(), password.getText().toString(), selectedImage, "user");
                user.type = "user";
                user.id = this.user.id;
                user.address=address.getText().toString();
                user.favourites=this.user.favourites;
                DataBaseHelper.updateUser(user);
                DataBaseHelper.saveUser(user);
                Toast.makeText(getActivity(), "تمت التعديلل بنجاح", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), UserMainActivity.class));
            }
        });


        return rootView;
    }


    private boolean validate() {
        if (Validate.isEmpty(name.getText().toString())
                || Validate.isEmpty(email.getText().toString())
                || Validate.isEmpty(phone.getText().toString())) {
            SweetDialogs.errorMessage(getActivity(), "من فضلك قم بملآ جميع البيانات");
            return false;

        } else if (!Validate.isAvLen(password.getText().toString(), 7, 100)) {
            SweetDialogs.errorMessage(getActivity(), "كلمة المرور يجب ان تكون اكبر من ٦ حروف او ارقام ");
            return false;
        } else if (DataBaseHelper.findChildCheck(email.getText().toString())) {
            SweetDialogs.errorMessage(getActivity(), "هذا المستخدم موجود من قبل");
            return false;
        }


        return true;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(getActivity(), data, "image",
                            43);
            volleyFileObjects.add(volleyFileObject);
            addServiceApi();
        } catch (Exception E) {
            E.getStackTrace();
        }


    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();
                ConnectionHelper.loadImage(image, selectedImage);
                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }

    User user;

    private void setData() {
        user = DataBaseHelper.getSavedUser();
        name.setText(user.name);
        address.setText(user.address);
        selectedImage = user.image;
        userName.setText(user.userName);
        email.setText(user.email);
        phone.setText(user.phone);
        password.setText(user.password);


        ConnectionHelper.loadImage(image, user.image);


    }

}