package com.yumaas.smartcity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.models.Chat;
import com.yumaas.smartcity.base.models.User;
import com.yumaas.smartcity.base.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class ChatAdapter2 extends RecyclerView.Adapter<ChatAdapter2.ViewHolder> {

    private Context context;
    ArrayList<Chat> chattersList;

    User user;
    public ChatAdapter2(Context context, ArrayList<Chat>chattersList) {
        this.context = context;
        this.chattersList = chattersList;
        user = DataBaseHelper.getSavedUser();
    }


    @Override
    public ChatAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        ChatAdapter2.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChatAdapter2.ViewHolder holder, final int position) {


        if(chattersList.get(position).reciver!=null&&(user.id==chattersList.get(position).reciver
                .id)) {
            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ConnectionHelper.loadImage(holder.image, chattersList.get(position).reciver.image);
        }else {
            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            ConnectionHelper.loadImage(holder.image, chattersList.get(position).reciver.image);

        }






        holder.message.setText(chattersList.get(position).message);
    }

    @Override
    public int getItemCount() {
        return chattersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;
        public ViewHolder(View itemView) {
            super(itemView);

            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.message);
            image = itemView.findViewById(R.id.image);

        }
    }
}
