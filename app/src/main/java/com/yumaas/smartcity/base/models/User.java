package com.yumaas.smartcity.base.models;

import android.util.Log;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.yumaas.smartcity.base.DataBaseHelper;

public class User implements Serializable {
    public int id;
    public String favourites="", name, userName, image, phone, email, password,type,files,location,address,facebook,insta;
    public double lat=0.0,lng=0.0;
    public int accepted;
    public String special,job;

    public User(String name, String userName, String phone, String email, String password, String image, String type) {
        this.id = DataBaseHelper.getCounter();
        this.name = name;
        this.image = image;
        this.userName = userName;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.type =type;
        this.accepted=0;
    }


    public void addFavourites(int id){
        User user =   DataBaseHelper.getSavedUser();
        if(favourites==null)favourites="";
        if(user.favourites==null)user.favourites="";
        if(!user.favourites.equals(""))
            user.favourites+=","+id;
        else user.favourites+=id;
        DataBaseHelper.updateUser(user);
        DataBaseHelper.saveUser(user);
    }

    public void deleteFavourites(int id){
        if(favourites==null)favourites="";

        User user =   DataBaseHelper.getSavedUser();
        String favs="";
        if(user.favourites==null)user.favourites="";
        List<String> list = Arrays.asList(user.favourites.split(","));
        for(int i=0; i<list.size(); i++){
            Log.d("TAGOZ2",list.get(i));
            if(!list.get(i).equals(id+"")){

                if(!favs.equals("")) {
                    favs += "," + list.get(i);
                }
                else{
                    favs+=list.get(i);
                }

            }
        }

        user.favourites=favs;
        DataBaseHelper.updateUser(user);
        DataBaseHelper.saveUser(user);
    }

    public boolean checkFavourites(int id){

        if(favourites==null)favourites="";

        User user =   DataBaseHelper.getSavedUser();
        if(user.favourites==null)user.favourites="";
        if(user.favourites!=null) {
            Log.d("TAGOZ",user.favourites);
            List<String>list = Arrays.asList(user.favourites.split(","));
            return list.contains(""+id);
        }
        else {
            return false;
        }


    }

}
