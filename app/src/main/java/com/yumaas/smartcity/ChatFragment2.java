package com.yumaas.smartcity;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.models.Chat;
import com.yumaas.smartcity.base.models.User;

import java.util.ArrayList;


public class ChatFragment2 extends Fragment {
    private View rootView;
    private RecyclerView menuList;
    ArrayList<Chat> chatters, chats;
    EditText nameEditText;
    Button chatBtn;
    User reciver,user;
    ChatAdapter2 chatAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        menuList = rootView.findViewById(R.id.menu_list);

        nameEditText = rootView.findViewById(R.id.et_chat_message);
        chatBtn = rootView.findViewById(R.id.btn_chat);


        chats = new ArrayList<>();
        chatters = DataBaseHelper
                .getDataLists().chats;
        user = DataBaseHelper.getSavedUser();


        Chat chat = (Chat) getArguments().getSerializable("chatItem");


        if(user.id==chat.sender.id){
            reciver=chat.reciver;
        }else {
            reciver=chat.sender;
        }

        for (int i = 0; i < chatters.size(); i++) {

            if ((chatters.get(i).sender.id == chat.sender.id &&
                    chatters.get(i).reciver.id == chat.reciver.id
            ) || (chatters.get(i).reciver.id == chat.sender.id &&
                    chatters.get(i).sender.id == chat.reciver.id)) ;
            chats.add(chatters.get(i));
        }
         chatAdapter = new ChatAdapter2(getActivity(), chats);
        menuList.setAdapter(chatAdapter);


        chatBtn.setOnClickListener(view -> {
            Chat chat1 = new Chat(user,reciver,nameEditText.getText().toString(), ChatFragment2.this.user.type);
            chats.add(chat1);
            DataBaseHelper.addChat(chat1);
            chatAdapter.notifyDataSetChanged();
            nameEditText.setText("");
        });

        return rootView;
    }


}
