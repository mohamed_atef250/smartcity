package com.yumaas.smartcity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class ChatUsersFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_admin_home_fragment, container, false);


        ArrayList<String> images = new ArrayList<>();
        images.add("https://images.squarespace-cdn.com/content/v1/5b8d399350a54f309702e849/1542734501243-GB0Y8LPKAASXZUXSS9OY/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0iyqMbMesKd95J-X4EagrgU9L3Sa3U8cogeb0tjXbfawd0urKshkc5MgdBeJmALQKw/to+all+the+nice+guys+finishing+last");
        images.add("https://pbs.twimg.com/profile_images/378800000048997257/b98dc5f0c8752b44457fc5f083ab2131_400x400.jpeg");
        images.add("https://cdn.vox-cdn.com/thumbor/Z-a4mQG4Ocb-Nx5_TT-3nVv_GLM=/0x0:1074x806/1200x675/filters:focal(487x364:657x534)/cdn.vox-cdn.com/uploads/chorus_image/image/67119463/richardcoffey.0.jpg");
        images.add("https://www.irishtimes.com/polopoly_fs/1.4020996.1568727860!/image/image.jpg_gen/derivatives/ratio_1x1_w1200/image.jpg");
        images.add("https://i.pinimg.com/736x/14/83/c4/1483c40268bb3ea250e20c7fa2b72b7d.jpg");

        images.add("https://hairstylecamp.com/wp-content/uploads/guys-with-blonde-highlights-8.jpg");
        images.add("https://static.independent.co.uk/s3fs-public/thumbnails/image/2018/08/01/19/gettyimages-459926482.jpg?width=982&height=726&auto=webp&quality=75");


        final ChatUsersAdapter chatUsersAdapter = new ChatUsersAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, images);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(chatUsersAdapter);


        return rootView;
    }
}