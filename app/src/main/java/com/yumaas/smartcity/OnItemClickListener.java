package com.yumaas.smartcity;

public interface OnItemClickListener {
    public void onItemClickListener(int position);
}
