package com.yumaas.smartcity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.like.LikeButton;
import com.like.OnLikeListener;
import com.squareup.picasso.Picasso;
import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.SweetDialogs;
import com.yumaas.smartcity.base.models.Chat;
import com.yumaas.smartcity.base.models.User;
import com.yumaas.smartcity.base.volleyutils.ConnectionHelper;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<User> places;
    User user;
    boolean isAdmin;

    public PlacesAdapter(OnItemClickListener onItemClickListener, ArrayList<User> places,boolean isAdmin) {
        this.onItemClickListener = onItemClickListener;
        this.places = places;
        this.isAdmin=isAdmin;
        try {
            user = DataBaseHelper.getSavedUser();
        }catch (Exception e){
            e.getStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return places == null ? 0 : places.size();
    }


    @Override
    public PlacesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_place, parent, false);
        PlacesAdapter.ViewHolder viewHolder = new PlacesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final PlacesAdapter.ViewHolder holder, final int position) {

        ConnectionHelper.loadImage(holder.imageView, places.get(position).image);
        holder.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps/@" + places.get(position).lat + "," + places.get(position).lng));
                    view.getContext().startActivity(intent);
                } catch (Exception e) {
                    e.getStackTrace();
                }
            }
        });

        holder.name.setText(places.get(position).email);
        holder.phone.setText(places.get(position).phone);
        holder.facebook.setText(places.get(position).facebook);
        holder.insta.setText(places.get(position).insta);


        if(places.get(position).accepted==1){
            holder.cardaccept.setVisibility(View.GONE);

        }else {
            holder.cardaccept.setVisibility(View.VISIBLE);

        }

       holder.fav.setLiked(user!=null&&user.checkFavourites(places.get(position).id));


        holder.files.setOnClickListener(view -> {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(places.get(position).files));
                view.getContext().startActivity(browserIntent);
            } catch (Exception e) {
                e.getStackTrace();

            }
        });



        holder.chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatFragment chatFragment =   new ChatFragment();
                Bundle bundle = new Bundle();
                Chat chat = new Chat(places.get(position),user,"","user");
                bundle.putSerializable("chatItem",chat);
                chatFragment.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(),chatFragment, "ChatFragment");
            }
        });

        if(!isAdmin) {
            holder.chat.setVisibility(View.VISIBLE);
            holder.accept.setVisibility(View.GONE);
            holder.reject.setVisibility(View.GONE);
            holder.delete.setVisibility(View.GONE);
            holder.fav.setVisibility(View.VISIBLE);
        }else {
            holder.accept.setVisibility(View.VISIBLE);
            holder.reject.setVisibility(View.VISIBLE);
            holder.delete.setVisibility(View.VISIBLE);
            holder.chat.setVisibility(View.GONE);
            holder.fav.setVisibility(View.GONE);
        }

        holder.accept.setOnClickListener(view -> SweetDialogs.successMessage(view.getContext(), "تم الموافقه بنجاح", sweetAlertDialog -> {
            places.get(position).accepted = 1;
            User user = places.get(position);
            DataBaseHelper.updateUser(user);
            notifyDataSetChanged();
        }));

        holder.reject.setOnClickListener(view -> SweetDialogs.successMessage(view.getContext(), "تم الرفض بنجاح", sweetAlertDialog -> {

            User user = places.get(position);
            DataBaseHelper.removeUser(user);
            places.remove(position);
            notifyDataSetChanged();
        }));

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("هل تريد الحذف")
                        .setContentText("بالتاكيد حذف الدائره ؟")
                        .setConfirmText("نعم احذف").setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlertDialog.cancel();
                    sweetAlertDialog.dismiss();
                    SweetDialogs.successMessage(view.getContext(), "تم الحذف بنجاح", sweetAlertDialog2 -> {
                        User user = places.get(position);
                        DataBaseHelper.removeUser(user);
                        places.remove(position);
                        notifyDataSetChanged();
                    });


                }).show();


            }
        });

        holder.fav.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {

                user.addFavourites(places.get(position).id);

            }

            @Override
            public void unLiked(LikeButton likeButton) {

                user.deleteFavourites(places.get(position).id);

            }
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, location,delete,chat;
        Button reject, accept;
        CardView cardreject,cardaccept;
        TextView name, phone, facebook, insta, files;
        LikeButton fav;
        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.image);
            delete = view.findViewById(R.id.delete);
            reject = view.findViewById(R.id.reject);
            accept = view.findViewById(R.id.accept);
            cardreject = view.findViewById(R.id.cardreject);
            cardaccept = view.findViewById(R.id.cardaccept);
            fav = view.findViewById(R.id.fav);

            location = view.findViewById(R.id.location);
            name = view.findViewById(R.id.name);
            phone = view.findViewById(R.id.phone);
            facebook = view.findViewById(R.id.facebook);
            insta = view.findViewById(R.id.insta);
            files = view.findViewById(R.id.files);
            chat=view.findViewById(R.id.chat);

        }
    }
}