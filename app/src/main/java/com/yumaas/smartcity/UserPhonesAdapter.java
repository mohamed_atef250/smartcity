package com.yumaas.smartcity;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.SweetDialogs;
import com.yumaas.smartcity.base.models.Place;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class UserPhonesAdapter extends RecyclerView.Adapter<UserPhonesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    boolean isAdmin;
    ArrayList<Place> places;

    public UserPhonesAdapter(OnItemClickListener onItemClickListener, ArrayList<Place> places, boolean isAdmin) {
        this.onItemClickListener = onItemClickListener;
        this.places = places;
        this.isAdmin = isAdmin;
    }


    @Override
    public int getItemCount() {
        return places == null ? 0 : places.size();
    }


    @Override
    public UserPhonesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_important_place, parent, false);
        UserPhonesAdapter.ViewHolder viewHolder = new UserPhonesAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserPhonesAdapter.ViewHolder holder, final int position) {

        Picasso.with(holder.itemView.getContext())
                .load(places.get(position).image)
                .into(holder.imageView);

        if (isAdmin) {
            holder.carddelete.setVisibility(View.VISIBLE);
        } else {
            holder.carddelete.setVisibility(View.GONE);
        }

        holder.callphone.setOnClickListener(view -> {
            try {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri
                        .parse("tel:" + places.get(position).phone));
                view.getContext().startActivity(intent);
            } catch (Exception e) {
                e.getStackTrace();
            }
        });


        holder.delete.setOnClickListener(view -> new SweetAlertDialog(view.getContext(),
                SweetAlertDialog.WARNING_TYPE)
                .setTitleText("هل تريد الحذف")
                .setContentText("بالتاكيد حذف الرقم ؟")
                .setConfirmText("نعم احذف")
                .setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlertDialog.cancel();
                    sweetAlertDialog.dismiss();

                    Place user = places.get(position);
                    DataBaseHelper.removeCenter(user);
                    places.remove(position);
                    notifyDataSetChanged();

                }).show());

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        Button callphone, delete;
        CardView cardcallphone, carddelete;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            callphone = view.findViewById(R.id.callphone);
            delete = view.findViewById(R.id.delete);
            cardcallphone = view.findViewById(R.id.cardcallphone);
            carddelete = view.findViewById(R.id.carddelete);

        }
    }
}