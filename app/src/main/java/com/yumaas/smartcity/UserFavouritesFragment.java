package com.yumaas.smartcity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class UserFavouritesFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_admin_home_fragment, container, false);


        ArrayList<String> images = new ArrayList<>();
        images.add("https://www.okaz.com.sa/uploads/images/2020/03/16/1527334.jpg");
        images.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSPwM0SqGeYOPPnyzYl6h5AwVJYIo7fSMPYNg&usqp=CAUhttps://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSPwM0SqGeYOPPnyzYl6h5AwVJYIo7fSMPYNg&usqp=CAU");
        images.add("https://pbs.twimg.com/profile_images/1330908049976995842/yM5Evi9a_400x400.jpg");
        images.add("https://files.elnashra.com/elnashra/pictures/6173024_1598628537.jpg");
        images.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSu6BGvCAFbNg_YQn2Cuu0jg_-owGUiyA2r6Q&usqp=CAU");


        final UserPlacesAdapter placesAdapter = new UserPlacesAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, images,true);
        final RecyclerView programsList = rootView.findViewById(R.id.places_list);
        ViewOperations.setRVHVertical(getActivity(), programsList);
        programsList.setAdapter(placesAdapter);


        return rootView;
    }
}