package com.yumaas.smartcity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.yumaas.smartcity.base.DataBaseHelper;
import com.yumaas.smartcity.base.SweetDialogs;
import com.yumaas.smartcity.base.filesutils.FileOperations;
import com.yumaas.smartcity.base.filesutils.VolleyFileObject;
import com.yumaas.smartcity.base.models.ImageResponse;
import com.yumaas.smartcity.base.models.Place;
import com.yumaas.smartcity.base.models.User;
import com.yumaas.smartcity.base.volleyutils.ConnectionHelper;
import com.yumaas.smartcity.base.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class UpdatePlaceProfile extends Fragment {

    View rootView;
    private String selectedImage;
    private ImageView  imageView ;
    private EditText name, userName, email, phone, password,files,location,address,facebook,insta;
    private TextView image;
    double lat=0.0, lng=0.0 ;
    Button add;
    ArrayList<VolleyFileObject> volleyFileObjects;
    User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.update_place_fragment, container, false);


        imageView=rootView.findViewById(R.id.imageView);
        files=rootView.findViewById(R.id.files);
        name = rootView.findViewById(R.id.name);
        userName = rootView.findViewById(R.id.user_name);
        email = rootView.findViewById(R.id.email);
        phone = rootView.findViewById(R.id.phone);
        files = rootView.findViewById(R.id.files);
        password = rootView.findViewById(R.id.password);
        location = rootView.findViewById(R.id.location);
        image = rootView.findViewById(R.id.image);
        location =   rootView.findViewById(R.id.location);
        address = rootView.findViewById(R.id.address);
        facebook = rootView.findViewById(R.id.facebook);
        insta = rootView.findViewById(R.id.instgram
        );

        add = rootView.findViewById(R.id.btn);

        setData();

        files.setFocusable(false);
        files.setClickable(true);


        image.setFocusable(false);
        image.setClickable(true);

        location.setFocusable(false);
        location.setClickable(true);



        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(),SelectLocationActivity.class);
                startActivityForResult(intent, 150);
            }
        });



        files.setOnClickListener(view -> {

            file = true;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        image.setOnClickListener(view -> {
            file = false;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });


        add.setOnClickListener(view -> {
            if(validate()){
                User user = new User(name.getText().toString(),
                        userName.getText().toString(),
                        phone.getText().toString(), email.getText().toString(),
                        password.getText().toString(),selectedImage,"place");

               user.id= this.user.id;
                user.address = address.getText().toString();
                user.files = selectedFile;
                user.facebook=facebook.getText().toString();
                user.insta = insta.getText().toString();
                user.lat=lat;
                user.lng=lng;
                user.address = address.getText().toString();

                DataBaseHelper.updateUser(user);
                DataBaseHelper.saveUser(user);
                Toast.makeText(getActivity(), "تمت التعديل بنجاح", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), ManagerMainActivity.class));
            }
        });


        return rootView;
    }

    private boolean validate(){
        if(Validate.isEmpty(name.getText().toString())
                ||Validate.isEmpty(email.getText().toString())
                ||Validate.isEmpty(phone.getText().toString())  ){
            SweetDialogs.errorMessage(getActivity(),"من فضلك قم بملآ جميع البيانات");
            return false;

        }else if(!Validate.isAvLen(password.getText().toString(),7,100)){
            SweetDialogs.errorMessage(getActivity(),"كلمة المرور يجب ان تكون اكبر من ٦ حروف او ارقام ");
            return  false;
        }else if(DataBaseHelper.findChildCheck(email.getText().toString())){
            SweetDialogs.errorMessage(getActivity(),"هذا المستخدم موجود من قبل");
            return  false;
        }


        return true;
    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            VolleyFileObject volleyFileObject = null;
            volleyFileObjects = new ArrayList<>();


            if (requestCode == 150) {
                location.setText("تم اختيار الموقع بنجاح");

                lat = data.getDoubleExtra("lat", 0.0);
                lng = data.getDoubleExtra("lng", 0.0);
            } else {

                try {

                    volleyFileObject =
                            FileOperations.getVolleyFileObject(getActivity(), data, "image",
                                    43);

                } catch (Exception E) {
                    E.getStackTrace();
                }

                volleyFileObjects.add(volleyFileObject);
                addServiceApi();
            }
        }catch (Exception e){
            e.getStackTrace();
        }

    }

    String selectedFile;
    boolean file;
    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;
                if(file){
                    selectedImage = imageResponse.getState();
                    files.setText("تم الاختيار بنجاح");
                }else {
                    selectedFile = imageResponse.getState();
                    ConnectionHelper.loadImage(imageView,selectedImage);
                }


                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }


    private void setData(){
         user = DataBaseHelper.getSavedUser();
        name.setText(user.name);
        selectedFile=user.files;
        selectedImage=user.image;
        userName.setText(user.userName);
        email.setText(user.email);
        phone.setText(user.phone);
        password.setText(user.password);

        files.setText("تغير ترخيص الدائره");
        location.setText("تغير الموقع");
        address.setText(user.address);
        facebook.setText(user.facebook);
        insta.setText(user.insta);

        ConnectionHelper.loadImage(imageView,user.image);


    }



}